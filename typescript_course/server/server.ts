import express from "express";

const server = express();
const port = 8080;

server.use(express.static("."));                       // serves the html, css, and favicon files
server.use(express.static("./js/app"));                // serves the game's js code in the js/app directory
server.use(express.json());                            // for parsing application/json
server.use(express.urlencoded({ extended: true }));    // for parsing application/x-www-form-urlencoded

server.get("/", (req, res) => {
    console.log("Recieved GET request");
    res.send("Hello World!");
})

// starts up server
server.listen(port, () => {
    console.log(`Multimath server listening on port ${port}`);
})


