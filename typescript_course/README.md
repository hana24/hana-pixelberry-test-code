# TypeScript-Getting-Started

This project is based on the example code from the Pluralsight courses "Typescript: Getting Started" and "Node.js: Getting Started". The goal of this project is to learn and get familiar with Typescript and Web concepts by creating a fully functional browser-based game using Typescript and HTML. When finished the game will prompt the player with a number of math problems to solve, read all of their inputs, and then assign a score based on their answers. In addition, I have created a Node.js server with the Express framework to serve the game app to the web browser. This is as opposed to the Webpack server that the Pluralsight coursed, which I changed in order to demonstrate Node.js concepts that I learned.

To run the project, first install the required node.js module dependencies using `npm install`.

![](./screenshots/npm_install.png)

This project uses the Express framework with Node.js to locally host a web server that can be accessed at http://localhost:8080. Running the `npm start` command runs a script that compiles the Typescript files into Javascript and then starts the Express server using this js code. The `tsc` compiler is configured to compile the project with two seperate tsconfig.json config files, one for the backend server and one for the frontend web app. This is because the server code compiles to CommonJS modules which are compatible with Node.js, but the browser code needs to use ES2015 modules to be able work in a web browser.

![](./screenshots/npm_start.png)

The start script uses `tsc-watch` to continuously watch the Typescript code for changes and re-compile them if needed. After successfully compiling the code, the script will restart the Express server, which then sends the newly compiled Javascript to the browser. The browser can then be refreshed to reload the webpage with this new Javascript code.

![](./screenshots/browser.png)

The webpage has three input fields, Player name, factor, and number of problems, and two buttons, the Start Game button and the Calculate Score button. The player can type their name, the multiplication factor to use, and the number of problems to solve in these input fields. Then, the player can click the Start Game button, which displays the questions for the player to answer.

![](./screenshots/start_game.png)

The player can input their answers to these problems in their respective input fields and then click the Calculate Score button. This button will calculate the number of questions the player answered correctly and then post their score on the scoreboard.

![](./screenshots/calculate_score.png)

