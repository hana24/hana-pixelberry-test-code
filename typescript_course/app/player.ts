export class Player  {
    name: string;
    age?: number;
    highScore?: number;

    constructor(name: string, age?: number, highScore?: number) {
        this.name = name;
        this.age = age;
        this.highScore = highScore;
    }

    formatName(): string {
        return this.name.toUpperCase();
    }
}