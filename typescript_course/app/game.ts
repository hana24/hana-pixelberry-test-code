import { Utility } from "./utility.js";
import { Result } from "./result.js";
import { Player } from "./player.js";
import { Scoreboard } from "./scoreboard.js";

export class Game {
    
    constructor(private scoreboard: Scoreboard, public player: Player, public problemCount: number, public factor: number) {
        
    }

    // show the html elements where the player can type in their answers
    displayGame(): void {
        let gameForm = "";
        for(let i = 1; i <= this.problemCount; i++) {
            gameForm += `<div class="form-group">`;
            gameForm += `<label for="answer${i}" class="col-sm-2 control-label">`;
            gameForm += `${this.factor} x ${i} = </label>`;
            gameForm += `<div class="col-sm-1"><input type = "text" class = "form-control" id="answer${i}" size="5" /></div>`;
            gameForm += `</div>`;
        }

        const gameElement: HTMLElement = document.getElementById("game")!;
        gameElement.innerHTML = gameForm;

        document.getElementById("calculate")!.removeAttribute("disabled");
    }

    // calculate the player's final score, and then add it to the scoreboard
    calculateScore(): void {
        let score = 0;

        // get the player's answers and compare each to the correct answer
        for(let i = 1; i <= this.problemCount; i++) {
            const playerAnswer = Number(Utility.getInputValue("answer" + i));

            // add one to score if the player got the right answer
            if(i * this.factor === playerAnswer) {
                score++;
            }
        }

        const result: Result = {
            playerName: this.player.name,
            score: score,
            problemCount: this.problemCount,
            factor: this.factor,
        }

        this.scoreboard.addResult(result);
        this.scoreboard.updateScoreboard();

        document.getElementById("calculate")!.setAttribute("disabled", "true");
    }
}