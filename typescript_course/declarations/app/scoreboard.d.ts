import { Result } from "./result";
export declare class Scoreboard {
    private results;
    addResult(result: Result): void;
    updateScoreboard(): void;
}
//# sourceMappingURL=scoreboard.d.ts.map