import { Player } from "./player.js";
import { Scoreboard } from "./scoreboard.js";
export declare class Game {
    private scoreboard;
    player: Player;
    problemCount: number;
    factor: number;
    constructor(scoreboard: Scoreboard, player: Player, problemCount: number, factor: number);
    displayGame(): void;
    calculateScore(): void;
}
//# sourceMappingURL=game.d.ts.map