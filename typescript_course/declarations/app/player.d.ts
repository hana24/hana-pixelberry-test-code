export declare class Player {
    name: string;
    age?: number;
    highScore?: number;
    constructor(name: string, age?: number, highScore?: number);
    formatName(): string;
}
//# sourceMappingURL=player.d.ts.map