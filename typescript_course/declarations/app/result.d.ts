export interface Result {
    playerName: string;
    score: number;
    problemCount: number;
    factor: number;
}
//# sourceMappingURL=result.d.ts.map