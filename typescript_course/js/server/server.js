"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server = express_1.default();
const port = 8080;
server.use(express_1.default.static(".")); // serves the html, css, and favicon files
server.use(express_1.default.static("./js/app")); // serves the game's js code in the js/app directory
server.use(express_1.default.json()); // for parsing application/json
server.use(express_1.default.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
server.get("/", (req, res) => {
    console.log("Recieved GET request");
    res.send("Hello World!");
});
// starts up server
server.listen(port, () => {
    console.log(`Multimath server listening on port ${port}`);
});
//# sourceMappingURL=server.js.map