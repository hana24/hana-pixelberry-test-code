import { Utility } from "./utility.js";
export class Game {
    constructor(scoreboard, player, problemCount, factor) {
        this.scoreboard = scoreboard;
        this.player = player;
        this.problemCount = problemCount;
        this.factor = factor;
    }
    // show the html elements where the player can type in their answers
    displayGame() {
        let gameForm = "";
        for (let i = 1; i <= this.problemCount; i++) {
            gameForm += `<div class="form-group">`;
            gameForm += `<label for="answer${i}" class="col-sm-2 control-label">`;
            gameForm += `${this.factor} x ${i} = </label>`;
            gameForm += `<div class="col-sm-1"><input type = "text" class = "form-control" id="answer${i}" size="5" /></div>`;
            gameForm += `</div>`;
        }
        const gameElement = document.getElementById("game");
        gameElement.innerHTML = gameForm;
        document.getElementById("calculate").removeAttribute("disabled");
    }
    // calculate the player's final score, and then add it to the scoreboard
    calculateScore() {
        let score = 0;
        // get the player's answers and compare each to the correct answer
        for (let i = 1; i <= this.problemCount; i++) {
            const playerAnswer = Number(Utility.getInputValue("answer" + i));
            // add one to score if the player got the right answer
            if (i * this.factor === playerAnswer) {
                score++;
            }
        }
        const result = {
            playerName: this.player.name,
            score: score,
            problemCount: this.problemCount,
            factor: this.factor,
        };
        this.scoreboard.addResult(result);
        this.scoreboard.updateScoreboard();
        document.getElementById("calculate").setAttribute("disabled", "true");
    }
}
//# sourceMappingURL=game.js.map