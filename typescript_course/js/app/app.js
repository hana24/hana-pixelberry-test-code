import { Utility } from "./utility.js";
import { Player } from "./player.js";
import { Scoreboard } from "./scoreboard.js";
import { Game } from "./game.js";
let game;
let scoreboard = new Scoreboard();
// intitializes the game using the player's inputs 
function startGame() {
    const playerName = Utility.getInputValue("playername");
    const problemCount = Number(Utility.getInputValue("problemCount"));
    const factor = Number(Utility.getInputValue("factor"));
    // validate inputs, don't start game if invalid
    if (problemCount > 0 && factor > 0) {
        const player = new Player(playerName);
        game = new Game(scoreboard, player, problemCount, factor);
        logGame(player, game);
        game.displayGame();
    }
    else {
        console.error("Invalid input");
    }
}
// writes details about a given game & its player to the console
function logGame(player, game) {
    console.log(`New game starting for player: ${player.name}, ${game.problemCount} problems of factor ${game.problemCount}`);
}
// starts the game when the player clicks the start button
document.getElementById("startGame").addEventListener("click", startGame);
document.getElementById("calculate").addEventListener("click", () => { game.calculateScore(); });
//# sourceMappingURL=app.js.map