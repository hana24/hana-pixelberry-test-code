export class Player {
    constructor(name, age, highScore) {
        this.name = name;
        this.age = age;
        this.highScore = highScore;
    }
    formatName() {
        return this.name.toUpperCase();
    }
}
//# sourceMappingURL=player.js.map