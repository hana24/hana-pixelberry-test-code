export class Utility {
    // reads the value of an html input field and returns it
    static getInputValue(elementID) {
        const inputElement = document.getElementById(elementID);
        if (inputElement.value === "") {
            return "Player";
        }
        else {
            return inputElement.value;
        }
    }
}
//# sourceMappingURL=utility.js.map