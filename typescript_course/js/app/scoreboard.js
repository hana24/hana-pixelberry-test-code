export class Scoreboard {
    constructor() {
        this.results = [];
    }
    addResult(result) {
        this.results.push(result);
    }
    updateScoreboard() {
        let output = "<h2>Scoreboard</h2>";
        for (let i = 0; i < this.results.length; i++) {
            const result = this.results[i];
            output += "<h4>";
            output += `${result.playerName}: ${result.score}/${result.problemCount} for factor ${result.factor}`;
            output += "</h4>";
        }
        const scoresElement = document.getElementById("scores");
        scoresElement.innerHTML = output;
    }
}
//# sourceMappingURL=scoreboard.js.map