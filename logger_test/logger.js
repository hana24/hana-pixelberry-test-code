"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Import:
const express_1 = __importDefault(require("express"));
// Declarations: 
const expressApp = (0, express_1.default)();
const port = 3000;
expressApp.use(express_1.default.json()); // for parsing application/json
expressApp.use(express_1.default.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
// Main: 
// define how server handles each HTTP request
expressApp.get('/', (req, res) => {
    console.log("Recieved GET request");
    console.log(req.query);
    res.send("Hello World!");
});
// frontend will send POST requests to the backend, recieve them here
expressApp.post('/', (req, res) => {
    console.log("Revieved POST request");
    console.log(req.query);
    console.log(req.body);
    // can save recieved log to a database or a file in here
    res.send("POST request recieved");
});
expressApp.put('/user', (req, res) => {
    console.log("Recieved PUT request at /user");
    res.send("PUT request recieved");
});
expressApp.delete('/user', (req, res) => {
    console.log("Recieved DELETE request at /user");
    res.send("DELETE request recieved");
});
// starts up server
expressApp.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
