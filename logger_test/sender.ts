// Import:
import axios from "axios";

// Declarations:
let defaultLog = console.log;
let defaultError = console.error;
// const args = process.argv.slice(2);

// redefine console.log to mimic original functionality and also send the log to the backend server
console.log = async function() {
    defaultLog("log sent: ", ...arguments);

    try {
        const response: any = await axios.post("http://localhost:3000/?testQuery=POST", {
            arguments: arguments,
            key2: "test 2",
        })
        defaultLog(response.data);
        defaultLog(response.status);
        defaultLog(response.statusText);
        defaultLog(response.headers);
    } 
    catch (error) {
        defaultError(error);
    }
    
    // // Old promise format?
    // axios.post("http://localhost:3000/?testQuery=POST", {
    //     arguments: arguments,
    //     key2: "test 2",
    // })
    // .then(function(response: any) {
    //     // handle success
    //     defaultLog(response.data);
    //     defaultLog(response.status);
    //     defaultLog(response.statusText);
    //     defaultLog(response.headers);
    // })
    // .catch(function(error: any) {
    //     // handle error
    //     defaultError(error);
    // })
}

// Main:
axios.get("http://localhost:3000/?testQuery=GET")
    .then(function(response: any) {
        // handle success
        defaultLog(response.data);
        defaultLog(response.status);
        defaultLog(response.statusText);
        defaultLog(response.headers);
        // console.log(response.config);
    })
    .catch(function(error: any) {
        // handle error
        defaultError(error);
    })

// test send some requests
console.log("test log", "test log 2");
console.log(1, 2, 3);
console.log("test log 1", 2, [3, 4, 5], { key3: "test 3"});
