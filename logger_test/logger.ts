// Import:
import express from "express";

// Declarations: 
const expressApp = express();
const port = 3000;

expressApp.use(express.json());                            // for parsing application/json
expressApp.use(express.urlencoded({ extended: true }));    // for parsing application/x-www-form-urlencoded

// Main: 
// define how server handles each HTTP request
expressApp.get('/', (req: any, res: any) => {
    console.log("Recieved GET request");
    console.log(req.query);
    res.send("Hello World!");
})

// frontend will send POST requests to the backend, recieve them here
expressApp.post('/', (req: any, res: any) => {
    console.log("Revieved POST request");

    console.log(req.query);
    console.log(req.body);

    // can save recieved log to a database or a file in here

    res.send("POST request recieved");
})

expressApp.put('/user', (req: any, res: any) => {
    console.log("Recieved PUT request at /user");
    res.send("PUT request recieved");
})

expressApp.delete('/user', (req: any, res: any) => {
    console.log("Recieved DELETE request at /user");
    res.send("DELETE request recieved");
})

// starts up server
expressApp.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})
