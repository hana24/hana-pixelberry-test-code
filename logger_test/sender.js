"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Import:
const axios_1 = __importDefault(require("axios"));
// Declarations:
let defaultLog = console.log;
let defaultError = console.error;
// const args = process.argv.slice(2);
// redefine console.log to mimic original functionality and also send the log to the backend server
console.log = function () {
    return __awaiter(this, arguments, void 0, function* () {
        defaultLog("log sent: ", ...arguments);
        try {
            const response = yield axios_1.default.post("http://localhost:3000/?testQuery=POST", {
                arguments: arguments,
                key2: "test 2",
            });
            defaultLog(response.data);
            defaultLog(response.status);
            defaultLog(response.statusText);
            defaultLog(response.headers);
        }
        catch (error) {
            defaultError(error);
        }
        // // Old promise format?
        // axios.post("http://localhost:3000/?testQuery=POST", {
        //     arguments: arguments,
        //     key2: "test 2",
        // })
        // .then(function(response: any) {
        //     // handle success
        //     defaultLog(response.data);
        //     defaultLog(response.status);
        //     defaultLog(response.statusText);
        //     defaultLog(response.headers);
        // })
        // .catch(function(error: any) {
        //     // handle error
        //     defaultError(error);
        // })
    });
};
// Main:
axios_1.default.get("http://localhost:3000/?testQuery=GET")
    .then(function (response) {
    // handle success
    defaultLog(response.data);
    defaultLog(response.status);
    defaultLog(response.statusText);
    defaultLog(response.headers);
    // console.log(response.config);
})
    .catch(function (error) {
    // handle error
    defaultError(error);
});
// test send some requests
console.log("test log", "test log 2");
console.log(1, 2, 3);
console.log("test log 1", 2, [3, 4, 5], { key3: "test 3" });
